const { expect } = require('chai');
const IO = require('../../service/IO');
const Mattermark = require('../../service/Mattermark');

describe('Participants test', () => {
  before(async () => {
    return IO.Init();
  });
  it('test getListOfCompanies (all)', async () => {
    const m = new Mattermark();
    const companyObject = await m.getListOfCompanies();
    await IO.Save('getListOfCompanies.all.json', companyObject);
    expect(companyObject).to.be.an('object');
    expect(companyObject.companies).to.be.an('array');
  });
  it('test getSpecificCompany (id)', async () => {
    const m = new Mattermark();
    const companyObject = await m.getSpecificCompany(74);
    await IO.Save('getSpecificCompany.74.json', companyObject);
    expect(companyObject).to.be.an('object');
  });
  it('test getRemainingQuota', async () => {
    const m = new Mattermark();
    const quotaObject = await m.getRemainingQuota();
    await IO.Save('getRemainingQuota.json', quotaObject);
    expect(quotaObject).to.be.an('array');
    expect(quotaObject).to.have.lengthOf(1);
    expect(quotaObject[0]).to.be.an('object');
  });
  it('test getListOfCompanies (custom)', async () => {
    const m = new Mattermark();
    const query = {
      employees: '100~', // 114215
    };
    const companyObject = await m.getListOfCompanies(query);
    await IO.Save('getListOfCompanies.custom.json', companyObject);
    expect(companyObject).to.be.an('object');
    expect(companyObject.companies).to.be.an('array');
  });
});
