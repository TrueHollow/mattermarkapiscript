# Parser

## System requirements:

Node.js version 10 or higher. 

## Prepare

`npm i --production`

## Run

`npm start`

## Support environment files with [`dotenv`](https://www.npmjs.com/package/dotenv#rules)

You can use a `.env` file to set environment variables:
```dotenv
MATTERMARK_API_KEY=some_api_key
# MATTERMARK_API_KEY=test_api_key
```
