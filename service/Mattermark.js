const request = require('request');
const logger = require('../logger')('service/Mattermark.js');
require('./dotenv_proxy');

const { MATTERMARK_API_KEY } = process.env;

if (typeof MATTERMARK_API_KEY !== 'string') {
  logger.error(
    'You should set environment variable MATTERMARK_API_KEY to use this API.'
  );
}

const headers = {
  Authorization: `Bearer ${MATTERMARK_API_KEY}`,
};

const HTTP_OK = 200;
const DEFAULT_ALL_QUERY = {
  // page: 2,
  per_page: 10000,
};

class Mattermark {
  async getListOfCompanies(q) {
    let query = q;
    if (query == null) {
      query = DEFAULT_ALL_QUERY;
    }
    logger.debug('getListOfCompanies (%s)', query ? JSON.stringify(query) : '');
    return new Promise((resolve, reject) => {
      const options = {
        method: 'GET',
        url: 'https://api.mattermark.com/companies',
        qs: query,
        headers,
        json: true,
      };
      request(options, (error, im, body) => {
        logger.debug(
          '...finished getListOfCompanies (%s)',
          query ? JSON.stringify(query) : ''
        );
        if (error) {
          return reject(error);
        }
        if (im.statusCode !== HTTP_OK) {
          return reject(
            new Error(
              `Response code: ${im.statusCode}, (${JSON.stringify(im.headers)})`
            )
          );
        }
        return resolve(body);
      });
    });
  }

  async getSpecificCompany(id) {
    logger.debug('getSpecificCompany with id (%s)...', id);
    return new Promise((resolve, reject) => {
      const url = `https://api.mattermark.com/companies/${id}`;
      const options = {
        method: 'GET',
        url,
        headers,
        json: true,
      };
      request(options, (error, im, body) => {
        logger.debug('...finished getSpecificCompany with id (%s)', id);
        if (error) {
          return reject(error);
        }
        if (im.statusCode !== HTTP_OK) {
          return reject(
            new Error(
              `Response code: ${im.statusCode}, (${JSON.stringify(im.headers)})`
            )
          );
        }
        return resolve(body);
      });
    });
  }

  async getRemainingQuota() {
    logger.debug('getRemainingQuota (%s)');
    return new Promise((resolve, reject) => {
      const options = {
        method: 'GET',
        url: 'https://api.mattermark.com/ratelimit/usage',
        headers,
        json: true,
      };
      request(options, (error, im, body) => {
        logger.debug('...finished getRemainingQuota');
        if (error) {
          return reject(error);
        }
        if (im.statusCode !== HTTP_OK) {
          return reject(
            new Error(
              `Response code: ${im.statusCode}, (${JSON.stringify(im.headers)})`
            )
          );
        }
        return resolve(body);
      });
    });
  }
}

module.exports = Mattermark;
