const fs = require('fs');
const path = require('path');
const logger = require('../logger')('service/IO.js');

const OUTPUT_DIRECTORY = path.resolve(__dirname, '..', 'output');

const createOutputDirectory = async () => {
  return new Promise((resolve, reject) => {
    fs.mkdir(OUTPUT_DIRECTORY, { recursive: true }, err => {
      if (err) {
        return reject(err);
      }
      logger.debug('Output directory was created.');
      return resolve();
    });
  });
};

class IO {
  static async Init() {
    return createOutputDirectory();
  }

  static async Save(fileName, data) {
    const fullPath = path.resolve(OUTPUT_DIRECTORY, fileName);
    return new Promise((resolve, reject) => {
      const str = JSON.stringify(data, null, '\t');
      fs.writeFile(fullPath, str, err => {
        if (err) {
          return reject(err);
        }
        return resolve();
      });
    });
  }
}

module.exports = IO;
